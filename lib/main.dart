import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Anime Season',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: RandomWords(),
    );
  }
}

Future<Map<dynamic, dynamic>> printDailyNewsDigest() async {
  final api = new Api();
  var newsDigest = await api.convert("tzPOhYPmMRJC", "tW_ThQJoP_Mm");
  Map<String, dynamic> anime = newsDigest;
  //print('${anime['title']}!');
  //print('We sent the verification link to ${user['email']}.');
  return anime;
}

class RandomWordsState extends State<RandomWords> {
  final Set<String> _saved = Set<String>();
  final TextStyle _biggerFont = TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Anime Season'),
        actions: <Widget>[
          // Add 3 lines from here...
          IconButton(icon: Icon(Icons.list), onPressed: _pushSaved),
        ], // ... to here.
      ),
      body: FutureBuilder<Map>(
          future: printDailyNewsDigest(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var anime = snapshot.data['title'];
              return ListView.builder(
                  padding: const EdgeInsets.all(16.0),
                  itemBuilder: /*1*/ (context, i) {
                    if (i < anime.length) {
                      return _buildRow(anime[i]["name"]);
                    }
                  });
            } else {
              return Text("Loading");
            }
          }),
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _saved.map(
            (String text) {
              return ListTile(
                title: Text(
                  text,
                  style: _biggerFont,
                ),
              );
            },
          );
          final List<Widget> divided = ListTile.divideTiles(
            context: context,
            tiles: tiles,
          ).toList();
          return Scaffold(
            appBar: AppBar(
              title: Text('Viendo esta temporada'),
            ),
            body: ListView(children: divided),
          );
        },
      ),
    );
  }

  Widget _buildRow(String text) {
    final bool alreadySaved = _saved.contains(text); // Add this line.
    return ListTile(
      title: Text(
        text,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            _saved.remove(text);
          } else {
            _saved.add(text);
          }
        });
      },
    );
  }
}

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();
}

class Api {
  final httpClient = HttpClient();
  final url = "www.parsehub.com";

  Future<dynamic> convert(String token, String key) async {
    final uri = Uri.https(
        url, '/api/v2/projects/$token/last_ready_run/data', {'api_key': key});

    final httpRequest = await httpClient.getUrl(uri);
    //print(uri);
    final httpResponse = await httpRequest.close();
    if (httpResponse.statusCode != HttpStatus.ok) {
      return null;
    }
    final responseBody = await httpResponse.transform(utf8.decoder).join();
    final jsonResponse = json.decode(responseBody);
    return jsonResponse;
  }
}
